package pt.ddias.francesinho.dto;

import jakarta.validation.constraints.*;
import lombok.Data;


import java.util.List;

@Data
public class FrancesinhaDto {
    @NotBlank
    private String name;

    @NotNull
    @Min(0)
    private Double price;

    @NotNull
    @Min(1)
    @Max(5)
    private Double rating;

    @NotEmpty
    private List<String> ingredientIds;

    private byte[] photo;

    @NotBlank
    private String restaurantId;
}
