package pt.ddias.francesinho.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class IngredientDto {
    @NotBlank
    private String name;
}
