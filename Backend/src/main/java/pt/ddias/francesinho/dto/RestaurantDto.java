package pt.ddias.francesinho.dto;

import jakarta.validation.constraints.*;
import lombok.Data;

@Data
public class RestaurantDto {
    @NotBlank
    private String name;

    @NotBlank
    private String address;

    @NotBlank
    private String city;

    @NotBlank
    private String country;

    @NotNull
    @Min(1)
    @Max(5)
    private Double rating;
}
