package pt.ddias.francesinho.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.ddias.francesinho.models.Ingredient;
import pt.ddias.francesinho.repositories.IngredientRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class IngredientService {

    private final IngredientRepository ingredientRepository;

    @Autowired
    public IngredientService(IngredientRepository ingredientRepository) {
        this.ingredientRepository = ingredientRepository;
    }

    public List<Ingredient> getAllIngredients() {
        return ingredientRepository.findByDeletedFalse();
    }

    public Optional<Ingredient> getIngredientById(String id) {
        return ingredientRepository.findById(id).filter(i -> !i.isDeleted());
    }

    public Ingredient saveIngredient(Ingredient ingredient) {
        ingredient.setDeleted(false);
        ingredient.setDeletedAt(null);
        return ingredientRepository.save(ingredient);
    }

    public void deleteIngredient(String id) {
        ingredientRepository.findById(id).ifPresent(ingredient -> {
            ingredient.setDeleted(true);
            ingredient.setDeletedAt(LocalDateTime.now());
            ingredientRepository.save(ingredient);
        });
    }

    public void restoreIngredient(String id) {
        ingredientRepository.findById(id).ifPresent(ingredient -> {
            ingredient.setDeleted(false);
            ingredient.setDeletedAt(null);
            ingredientRepository.save(ingredient);
        });
    }

    public Ingredient updateIngredient(Ingredient ingredient) {
        return ingredientRepository.save(ingredient);
    }
}
