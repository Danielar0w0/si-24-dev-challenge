package pt.ddias.francesinho.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.ddias.francesinho.models.Restaurant;
import pt.ddias.francesinho.repositories.RestaurantRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class RestaurantService {

    private final RestaurantRepository restaurantRepository;

    @Autowired
    public RestaurantService(RestaurantRepository restaurantRepository) {
        this.restaurantRepository = restaurantRepository;
    }

    public List<Restaurant> getAllRestaurants() {
        return restaurantRepository.findByDeletedFalse();
    }

    public List<Restaurant> getAllDeletedRestaurants() {
        return restaurantRepository.findByDeletedTrue();
    }

    public Optional<Restaurant> getRestaurantById(String id) {
        return restaurantRepository.findById(id).filter(r -> !r.isDeleted());
    }

    public Restaurant saveRestaurant(Restaurant restaurant) {
        restaurant.setDeleted(false);
        restaurant.setDeletedAt(null);
        return restaurantRepository.save(restaurant);
    }

    public void deleteRestaurant(String id) {
        restaurantRepository.findById(id).ifPresent(restaurant -> {
            restaurant.setDeleted(true);
            restaurant.setDeletedAt(LocalDateTime.now());
            restaurantRepository.save(restaurant);
        });
    }

    public void restoreRestaurant(String id) {
        restaurantRepository.findById(id).ifPresent(restaurant -> {
            restaurant.setDeleted(false);
            restaurant.setDeletedAt(null);
            restaurantRepository.save(restaurant);
        });
    }

    public Restaurant updateRestaurant(Restaurant restaurant) {
        return restaurantRepository.save(restaurant);
    }
}
