package pt.ddias.francesinho.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.ddias.francesinho.models.Francesinha;
import pt.ddias.francesinho.repositories.FrancesinhaRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class FrancesinhaService {

    private final FrancesinhaRepository francesinhaRepository;

    @Autowired
    public FrancesinhaService(FrancesinhaRepository francesinhaRepository) {
        this.francesinhaRepository = francesinhaRepository;
    }

    public List<Francesinha> getAllFrancesinhas() {
        return francesinhaRepository.findByDeletedFalse();
    }

    public List<Francesinha> getAllDeletedFrancesinhas() {
        return francesinhaRepository.findByDeletedTrue();
    }

    public Optional<Francesinha> getFrancesinhaById(String id) {
        return francesinhaRepository.findById(id).filter(f -> !f.isDeleted());
    }

    public Optional<Francesinha> getFrancesinhaByIdAndDeleted(String id) {
        return francesinhaRepository.findById(id);
    }

    public Francesinha saveFrancesinha(Francesinha francesinha) {
        francesinha.setDeleted(false);
        francesinha.setDeletedAt(null);
        return francesinhaRepository.save(francesinha);
    }

    public void deleteFrancesinha(String id) {
        francesinhaRepository.findById(id).ifPresent(francesinha -> {
            francesinha.setDeleted(true);
            francesinha.setDeletedAt(LocalDateTime.now());
            francesinhaRepository.save(francesinha);
        });
    }

    public void restoreFrancesinha(String id) {
        francesinhaRepository.findById(id).ifPresent(francesinha -> {
            francesinha.setDeleted(false);
            francesinha.setDeletedAt(null);
            francesinhaRepository.save(francesinha);
        });
    }

    public Francesinha updateFrancesinha(Francesinha francesinha) {
        return francesinhaRepository.save(francesinha);
    }

    public List<Francesinha> getFrancesinhasByRestaurantId(String id) {
        return francesinhaRepository.findByRestaurantIdAndDeletedFalse(id);
    }

    public List<Francesinha> getAllFrancesinhasByRestaurantId(String id) {
        return francesinhaRepository.findByRestaurantId(id);
    }
}