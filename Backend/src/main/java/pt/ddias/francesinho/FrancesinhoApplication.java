package pt.ddias.francesinho;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FrancesinhoApplication {

	public static void main(String[] args) {
		SpringApplication.run(FrancesinhoApplication.class, args);
	}

}
