package pt.ddias.francesinho.models;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Document(collection = "francesinhas")
public class Francesinha {
    @Id
    private String id;
    private String name;
    private double price;
    private double rating;

    @DBRef
    private List<Ingredient> ingredients;

    private byte[] photo;

    @DBRef
    private Restaurant restaurant;

    private boolean deleted = false;
    private LocalDateTime deletedAt;
}