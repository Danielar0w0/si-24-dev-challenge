package pt.ddias.francesinho.models;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Data
@Document(collection = "ingredients")
public class Ingredient {
    @Id
    private String id;
    private String name;

    private boolean deleted = false;
    private LocalDateTime deletedAt;
}
