package pt.ddias.francesinho.models;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Data
@Document(collection = "restaurants")
public class Restaurant {
    @Id
    private String id;
    private String name;
    private String address;
    private String city;
    private String country;
    private double rating;

//    @DBRef
//    private List<Francesinha> francesinhas;

    private boolean deleted = false;
    private LocalDateTime deletedAt;
}
