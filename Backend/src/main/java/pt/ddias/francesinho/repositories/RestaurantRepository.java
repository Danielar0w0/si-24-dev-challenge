package pt.ddias.francesinho.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import pt.ddias.francesinho.models.Restaurant;

import java.util.List;

public interface RestaurantRepository extends MongoRepository<Restaurant, String> {
    List<Restaurant> findByDeletedFalse();

    List<Restaurant> findByDeletedTrue();
}