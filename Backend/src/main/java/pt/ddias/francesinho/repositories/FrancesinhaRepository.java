package pt.ddias.francesinho.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import pt.ddias.francesinho.models.Francesinha;

import java.util.List;

public interface FrancesinhaRepository extends MongoRepository<Francesinha, String> {
    List<Francesinha> findByDeletedFalse();

    List<Francesinha> findByDeletedTrue();

    List<Francesinha> findByRestaurantId(String restaurantId);

    List<Francesinha> findByRestaurantIdAndDeletedFalse(String restaurantId);

}
