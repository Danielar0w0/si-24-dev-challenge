package pt.ddias.francesinho.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import pt.ddias.francesinho.models.Ingredient;

import java.util.List;

public interface IngredientRepository extends MongoRepository<Ingredient, String> {
    List<Ingredient> findByDeletedFalse();
}
