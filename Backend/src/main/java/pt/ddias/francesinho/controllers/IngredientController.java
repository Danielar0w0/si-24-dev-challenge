package pt.ddias.francesinho.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pt.ddias.francesinho.models.Ingredient;
import pt.ddias.francesinho.services.IngredientService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/ingredients")
public class IngredientController {

    private final IngredientService ingredientService;

    @Autowired
    public IngredientController(IngredientService ingredientService) {
        this.ingredientService = ingredientService;
    }

    @GetMapping
    public List<Ingredient> getAllIngredients() {
        return ingredientService.getAllIngredients();
    }

    @GetMapping("/{id}")
    public Optional<Ingredient> getIngredientById(@PathVariable String id) {
        return ingredientService.getIngredientById(id);
    }

    @PostMapping
    public Ingredient createIngredient(@RequestBody Ingredient ingredient) {
        // Don't allow to create an ingredient with the same name
        if (ingredientService.getAllIngredients().stream().anyMatch(i -> i.getName().equals(ingredient.getName()))) {
            throw new IllegalArgumentException("Ingredient already exists");
        }
        return ingredientService.saveIngredient(ingredient);
    }

    @PutMapping("/{id}")
    public Ingredient updateIngredient(@PathVariable String id, @RequestBody Ingredient ingredient) {
        // Don't allow to update an ingredient with the same name
        if (ingredientService.getAllIngredients().stream().anyMatch(i -> i.getName().equals(ingredient.getName()))) {
               throw new IllegalArgumentException("Ingredient already exists");
        }

        ingredient.setId(id);
        return ingredientService.updateIngredient(ingredient);
    }

    @DeleteMapping("/{id}")
    public void deleteIngredient(@PathVariable String id) {
        ingredientService.deleteIngredient(id);
    }

    @PutMapping("/restore/{id}")
    public void restoreIngredient(@PathVariable String id) {
        ingredientService.restoreIngredient(id);
    }
}
