package pt.ddias.francesinho.controllers;

import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pt.ddias.francesinho.dto.RestaurantDto;
import pt.ddias.francesinho.models.Francesinha;
import pt.ddias.francesinho.models.Restaurant;
import pt.ddias.francesinho.services.FrancesinhaService;
import pt.ddias.francesinho.services.RestaurantService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/restaurants")
public class RestaurantController {

    private final RestaurantService restaurantService;

    private final FrancesinhaService francesinhaService;

    @Autowired
    public RestaurantController(RestaurantService restaurantService, FrancesinhaService francesinhaService) {
        this.restaurantService = restaurantService;
        this.francesinhaService = francesinhaService;
    }

    @GetMapping
    public List<Restaurant> getAllRestaurants() {
        return restaurantService.getAllRestaurants();
    }

    @GetMapping("/deleted")
    public List<Restaurant> getAllDeletedRestaurants() {
        return restaurantService.getAllDeletedRestaurants();
    }

    @GetMapping("/{id}")
    public Optional<Restaurant> getRestaurantById(@PathVariable String id) {
        return restaurantService.getRestaurantById(id);
    }

    @GetMapping("/{id}/francesinhas")
    public List<Francesinha> getFrancesinhasByRestaurantId(@PathVariable String id) {
        return francesinhaService.getFrancesinhasByRestaurantId(id);
    }

    @PostMapping
    public Restaurant createRestaurant(@Valid @RequestBody RestaurantDto restaurantDto) {
        Restaurant restaurant = new Restaurant();
        restaurant.setName(restaurantDto.getName());
        restaurant.setAddress(restaurantDto.getAddress());
        restaurant.setCity(restaurantDto.getCity());
        restaurant.setCountry(restaurantDto.getCountry());
        restaurant.setRating(restaurantDto.getRating());

        return restaurantService.saveRestaurant(restaurant);
    }

    @PutMapping("/{id}")
    public Restaurant updateRestaurant(@PathVariable String id, @Valid @RequestBody RestaurantDto restaurantDto) {
        Restaurant restaurant = restaurantService.getRestaurantById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid restaurant ID"));

        restaurant.setName(restaurantDto.getName());
        restaurant.setAddress(restaurantDto.getAddress());
        restaurant.setCity(restaurantDto.getCity());
        restaurant.setCountry(restaurantDto.getCountry());
        restaurant.setRating(restaurantDto.getRating());

        return restaurantService.updateRestaurant(restaurant);
    }

    @DeleteMapping("/{id}")
    public void deleteRestaurant(@PathVariable String id) {
        restaurantService.deleteRestaurant(id);

        // Delete all francesinhas from this restaurant
        francesinhaService.getFrancesinhasByRestaurantId(id).forEach(francesinha -> francesinhaService.deleteFrancesinha(francesinha.getId()));
    }

    @PutMapping("/restore/{id}")
    public void restoreRestaurant(@PathVariable String id) {
        restaurantService.restoreRestaurant(id);

        // Restore all francesinhas from this restaurant
        francesinhaService.getAllFrancesinhasByRestaurantId(id).forEach(francesinha -> francesinhaService.restoreFrancesinha(francesinha.getId()));
    }
}
