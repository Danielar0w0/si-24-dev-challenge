package pt.ddias.francesinho.controllers;

import com.google.gson.Gson;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pt.ddias.francesinho.dto.FrancesinhaDto;
import pt.ddias.francesinho.models.Francesinha;
import pt.ddias.francesinho.models.Ingredient;
import pt.ddias.francesinho.models.Restaurant;
import pt.ddias.francesinho.services.FrancesinhaService;
import pt.ddias.francesinho.services.IngredientService;
import pt.ddias.francesinho.services.RestaurantService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/francesinhas")
public class FrancesinhaController {

    private final FrancesinhaService francesinhaService;

    private final IngredientService ingredientService;

    private final RestaurantService restaurantService;

    @Autowired
    public FrancesinhaController(FrancesinhaService francesinhaService, IngredientService ingredientService, RestaurantService restaurantService) {
        this.francesinhaService = francesinhaService;
        this.ingredientService = ingredientService;
        this.restaurantService = restaurantService;
    }

    @GetMapping
    public List<Francesinha> getAllFrancesinhas() {
        return francesinhaService.getAllFrancesinhas();
    }

    @GetMapping("/deleted")
    public List<Francesinha> getAllDeletedFrancesinhas() {
        return francesinhaService.getAllDeletedFrancesinhas();
    }

    @GetMapping("/{id}")
    public Optional<Francesinha> getFrancesinhaById(@PathVariable String id) {
        return francesinhaService.getFrancesinhaById(id);
    }

    @PostMapping
    public Francesinha createFrancesinha(@Valid @RequestParam("francesinha") String francesinhaJson, @RequestPart(value = "photo", required = false) MultipartFile photo) {

        // Deserialize the JSON string to a FrancesinhaDto object (with Gson)
        FrancesinhaDto francesinhaDto = new Gson().fromJson(francesinhaJson, FrancesinhaDto.class);

        Francesinha francesinha = new Francesinha();
        francesinha.setName(francesinhaDto.getName());
        francesinha.setPrice(francesinhaDto.getPrice());
        francesinha.setRating(francesinhaDto.getRating());

        List<Ingredient> ingredients = francesinhaDto.getIngredientIds().stream()
                .map(ingredientService::getIngredientById)
                .map(Optional::orElseThrow)
                .collect(Collectors.toList());
        francesinha.setIngredients(ingredients);

        Restaurant restaurant = restaurantService.getRestaurantById(francesinhaDto.getRestaurantId())
                .orElseThrow(() -> new IllegalArgumentException("Invalid restaurant ID"));
        francesinha.setRestaurant(restaurant);

        // If the photo is not null, set the photo
        try {
            if (!photo.isEmpty()) {
                francesinha.setPhoto(photo.getBytes());
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("Invalid photo");
        }

        return francesinhaService.saveFrancesinha(francesinha);
    }

    @PutMapping("/{id}")
    public Francesinha updateFrancesinha(@PathVariable String id, @Valid @RequestParam("francesinha") String francesinhaJson, @RequestPart(value = "photo", required = false) MultipartFile photo) {

        Francesinha francesinha = francesinhaService.getFrancesinhaById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid francesinha ID"));

        // Deserialize the JSON string to a FrancesinhaDto object (with Gson)
        FrancesinhaDto francesinhaDto = new Gson().fromJson(francesinhaJson, FrancesinhaDto.class);

        francesinha.setName(francesinhaDto.getName());
        francesinha.setPrice(francesinhaDto.getPrice());
        francesinha.setRating(francesinhaDto.getRating());

        // If the photo is not null, set the photo
        try {
            if (!photo.isEmpty()) {
                francesinha.setPhoto(photo.getBytes());
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("Invalid photo");
        }

        List<Ingredient> ingredients = francesinhaDto.getIngredientIds().stream()
                .map(ingredientService::getIngredientById)
                .map(Optional::orElseThrow)
                .collect(Collectors.toList());
        francesinha.setIngredients(ingredients);

        Restaurant restaurant = restaurantService.getRestaurantById(francesinhaDto.getRestaurantId())
                .orElseThrow(() -> new IllegalArgumentException("Invalid restaurant ID"));
        francesinha.setRestaurant(restaurant);

        return francesinhaService.updateFrancesinha(francesinha);
    }

    @DeleteMapping("/{id}")
    public void deleteFrancesinha(@PathVariable String id) {
        francesinhaService.deleteFrancesinha(id);
    }

    @PutMapping("/restore/{id}")
    public void restoreFrancesinha(@PathVariable String id) {

        // Obtain the francesinha by id
        Francesinha francesinha = francesinhaService.getFrancesinhaByIdAndDeleted(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid francesinha ID"));

        // Check if the francesinha's restaurant is deleted
        if (francesinha.getRestaurant().isDeleted()) {
            throw new IllegalArgumentException("Restaurant is deleted");
        }

        francesinhaService.restoreFrancesinha(id);
    }
}