# Francesinha Wiki 🥪

Francesinha Wiki is a dynamic web platform that allows users to explore, review, and manage entries related to the iconic Portuguese dish, the Francesinha, along with related restaurant information. This application is built using React, Tailwind CSS, Daisy UI on the frontend, and Spring Boot with MongoDB for the backend.

## Main Features

The platform enables users to:

- **Register new francesinhas**
- **List registered francesinhas**
- **Display details of a registered Francesinha**
- **Update francesinha details**
- **Delete francesinha entries**
- **Register new restaurants**
- **List existing restaurants**
- **Show details of an existing restaurant along with its available francesinhas**
- **Update existing restaurants**
- **Delete restaurants**
- **Search for restaurants and francesinhas**

### Additional Features

- **Soft deletion** of francesinhas and restaurants with the ability to recover them.
- **Ingredient Management:** Ingredients are treated as separate entities/classes that can be associated with multiple francesinhas.
- **Validation of Input:** Ensures that all backend inputs are validated to receive only the parameters needed.
- **Form Validations:** Frontend forms are validated to ensure data integrity.
- **Sorting:** Users can sort francesinhas and restaurants by name and rating.
- **Photo Uploads:** Users can upload photos for each francesinha entry.

## Project Setup

### Prerequisites

- Node.js and npm (https://nodejs.org/)
- Java JDK 11 or newer (https://openjdk.java.net/)
- MongoDB (https://www.mongodb.com/try/download/community)

### Backend Setup

1. **Navigate to the backend directory:**

   ```bash
   cd Backend
   ```

2. **Build the project:**

   ```bash
   ./mvnw clean install
   ```

3. **Run the Spring Boot application:**

   ```bash
   ./mvnw spring-boot:run
   ```

   This will start the server on `http://localhost:8080`.

### Frontend Setup

1. **Navigate to the frontend directory:**

   ```bash
   cd Frontend
   ```

2. **Install dependencies:**

   ```bash
   npm install
   ```

3. **Run the React application:**

   ```bash
   npm run dev
   ```

   This will open the application on `http://localhost:5173`.

### MongoDB

Ensure MongoDB is running locally on its default port (`27017`). Update the MongoDB URI in the backend's `application.properties` if your setup differs.

## Running the Application

Once both the backend and frontend are running, navigate to `http://localhost:5173` in your browser to start exploring the Francesinha Wiki.

## Future Work

- **Response Entity**: Refactor the backend to return ResponseEntity objects for better error handling.