import {api, apiMultipart} from "./api.jsx";

// Get all francesinhas
export const getFrancesinhas = async () => {
    const response = await api.get('/francesinhas');
    return response.data;
}

// Get all deleted francesinhas
export const getDeletedFrancesinhas = async () => {
    const response = await api.get('/francesinhas/deleted');
    return response.data;
}

// Get a single francesinha by ID
export const getFrancesinha = async (id) => {
    const response = await api.get(`/francesinhas/${id}`);
    return response.data;
}

// Create a new francesinha
export const createFrancesinha = async (data) => {
    const response = await apiMultipart.post('/francesinhas', data);
    return response.data;
}

// Update an existing francesinha
export const updateFrancesinha = async (data) => {
    const response = await apiMultipart.put(`/francesinhas/${data.id}`, data.formData);
    return response.data;
}

// Delete a francesinha
export const deleteFrancesinha = async (id) => {
    const response = await api.delete(`/francesinhas/${id}`);
    return response.data;
}

// Restore a deleted francesinha
export const restoreFrancesinha = async (id) => {
    const response = await api.put(`/francesinhas/restore/${id}`);
    return response.data;
}