import { api } from "./api.jsx";

// Get all restaurants
export const getRestaurants = async () => {
    const response = await api.get('/restaurants');
    return response.data;
}

// Get a single restaurant by ID
export const getRestaurant = async (id) => {
    const response = await api.get(`/restaurants/${id}`);
    return response.data;
}

// Create a new restaurant
export const createRestaurant = async (restaurantDto) => {
    const response = await api.post('/restaurants', restaurantDto);
    return response.data;
}

// Update an existing restaurant
export const updateRestaurant = async (data) => {
    const response = await api.put(`/restaurants/${data.id}`, data.restaurant);
    return response.data;
}

// Delete a restaurant
export const deleteRestaurant = async (id) => {
    const response = await api.delete(`/restaurants/${id}`);
    return response.data;
}

// Restore a deleted restaurant
export const restoreRestaurant = async (id) => {
    const response = await api.put(`/restaurants/restore/${id}`);
    return response.data;
}

// Get all francesinhas
export const getRestaurantFrancesinhas = async (id) => {
    const response = await api.get(`/restaurants/${id}/francesinhas`);
    return response.data;
}

// Get all deleted restaurants
export const getDeletedRestaurants = async () => {
    const response = await api.get('/restaurants/deleted');
    return response.data;
}