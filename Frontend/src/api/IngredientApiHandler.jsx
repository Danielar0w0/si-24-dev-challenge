import { api } from './api';

// Get all ingredients
export const getIngredients = async () => {
    const response = await api.get('/ingredients');
    return response.data;
}

// Get a single ingredient by ID
export const getIngredient = async (id) => {
    const response = await api.get(`/ingredients/${id}`);
    return response.data;
}

// Create a new ingredient
export const createIngredient = async (ingredientDto) => {
    const response = await api.post('/ingredients', ingredientDto);
    return response.data;
}

// Update an existing ingredient
export const updateIngredient = async (id, ingredientDto) => {
    const response = await api.put(`/ingredients/${id}`, ingredientDto);
    return response.data;
}

// Delete an ingredient
export const deleteIngredient = async (id) => {
    const response = await api.delete(`/ingredients/${id}`);
    return response.data;
}

// Restore a deleted ingredient
export const restoreIngredient = async (id) => {
    const response = await api.put(`/ingredients/restore/${id}`);
    return response.data;
}