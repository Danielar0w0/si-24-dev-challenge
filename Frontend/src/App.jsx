import './App.css'
import Navbar from "./components/Navbar.jsx";
import {Outlet} from "react-router-dom";
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

function App() {

    return (
        <div className="flex flex-col min-h-screen">
            <Navbar/>
            <Outlet/>
            <ToastContainer className="z-10"
                position="bottom-right"
                autoClose={3000}
                hideProgressBar={true}
                newestOnTop={false}
                closeOnClick={true}
                rtl={false}
                pauseOnFocusLoss={false}
                draggable={true}
                pauseOnHover={true}
                closeButton={false}
            />
        </div>
    )
}

export default App
