import {IoCloseCircle} from "react-icons/io5";
import {useUpdateFrancesinha} from "../../hooks/useFrancesinhas.jsx";
import PropTypes from "prop-types";
import {useState} from "react";
import FrancesinhaForm from "./FrancesinhaForm.jsx";

const UpdateFrancesinhaModal = ({ currentFrancesinha }) => {

    const updateFrancesinhaMutation = useUpdateFrancesinha();

    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);

    const closeModal = (e) => {
        e.preventDefault();

        setLoading(false);
        document.getElementById("update_francesinha_modal").close();
    }

    const submitFrancesinha = (francesinha) => {

        setError(null);

        if (francesinha.name === "" || francesinha.price === "" || francesinha.ingredients === "") {
            setTimeout(() => setError("Please fill out all fields"), 200);
            return;
        }

        // Check if rating is between 1 and 5
        if (francesinha.rating < 1 || francesinha.rating > 5) {
            setTimeout(() => setError("Rating must be between 1 and 5"), 200);
            return;
        }

        setLoading(true);

        const finalFrancesinha = {
            name: francesinha.name,
            price: francesinha.price,
            rating: francesinha.rating,
            ingredientIds: francesinha.ingredients.map(ingredient => ingredient.id),
            restaurantId: francesinha.restaurant.id
        }

        const formData = new FormData();
        formData.append("francesinha", JSON.stringify(finalFrancesinha)); // Append the francesinha object to the form data
        formData.append("photo", francesinha.file); // Append the file to the form data


        updateFrancesinhaMutation.mutate({
            id: currentFrancesinha.id,
            formData: formData
        });

        setLoading(false);
    }

    return (
        <dialog id="update_francesinha_modal" className="modal modal-bottom sm:modal-middle">
            <div className="modal-box p-12 flex flex-col">
                <form method="dialog" className="modal-close" onClick={closeModal}>
                    <button className="btn btn-sm btn-circle btn-ghost absolute right-4 top-4">✕</button>
                </form>
                <div className="flex flex-col gap-4">
                    <h1 className="text-5xl font-bold">Update Restaurant</h1>
                    <p className="opacity-50 text-sm mb-4">Fill out the form to update the restaurant.</p>

                    {loading &&
                        <div className="flex items-center gap-2 p-4 rounded-xl w-full bg-base-200/30 font-semibold">
                            <span className="loading loading-spinner loading-sm"></span>
                            <p>Updating restaurant...</p>
                        </div>
                    }
                    {error &&
                        <div
                            className="flex flex-row items-center gap-2 p-4 rounded-xl w-full bg-base-200/30 font-semibold">
                            <IoCloseCircle className="w-6 h-6 text-red-500"/> {error}
                        </div>
                    }

                    <FrancesinhaForm currentFrancesinha={currentFrancesinha} closeModal={closeModal}
                                     submitFrancesinha={submitFrancesinha} />
                </div>
            </div>
            <form method="dialog" className="modal-backdrop fixed top-0 left-0 w-full h-full"
                  onClick={closeModal}>
                <button>close</button>
            </form>
        </dialog>
    );

}

// Props Validation
UpdateFrancesinhaModal.propTypes = {
    currentFrancesinha: PropTypes.object.isRequired
}

export default UpdateFrancesinhaModal;