import PropTypes from "prop-types";
import FrancesinhaCard from "./FrancesinhaCard.jsx";

const FrancesinhaList = ({francesinhas}) => {

    return (
        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-12">
            {francesinhas.map(francesinha => (
                <FrancesinhaCard key={francesinha.id} francesinha={francesinha}/>
            ))}

            {francesinhas.length === 0 && <div className="opacity-50 text-sm">No francesinhas found.</div>}
        </div>
    )
}

// Props Validation
FrancesinhaList.propTypes = {
    francesinhas: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        rating: PropTypes.number.isRequired,
        ingredients: PropTypes.array.isRequired,
        restaurant: PropTypes.object.isRequired
    })).isRequired
}

export default FrancesinhaList;