import {FaStar} from "react-icons/fa";
import {Link} from "react-router-dom";
import PropTypes from "prop-types";

const FrancesinhaCard = ({francesinha}) => {

    return (
        <Link to={"/francesinhas/" + francesinha.id}
              className="flex flex-col gap-4 rounded-lg cursor-pointer ">
            <div className="w-full h-56 bg-base-200 rounded-t-lg flex items-end justify-end gap-1 p-3"
                    style={{backgroundImage: `url(data:image/jpeg;base64,${francesinha.photo})`, backgroundSize: "cover"}}>
                <div className="bg-base-100/80 rounded-lg p-2">
                    <div className="flex gap-1 items-center font-bold text-sm">{francesinha.price}€</div>
                </div>

                <div className="bg-base-100/80 rounded-lg p-2">
                    <div className="flex gap-1 items-center font-bold text-sm">{francesinha.rating.toFixed(1)} <FaStar /></div>
                </div>
            </div>
            <div className="flex flex-col gap-1">
                <h2 className="text-3xl font-bold">{francesinha.name}</h2>
                <p className="text-xs">{francesinha.restaurant.name}</p>
            </div>
        </Link>
    )

}

// Props Validation
FrancesinhaCard.propTypes = {
    francesinha: PropTypes.object.isRequired
}

export default FrancesinhaCard;
