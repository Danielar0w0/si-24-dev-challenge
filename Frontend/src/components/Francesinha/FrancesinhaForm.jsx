import RestaurantInput from "../Restaurant/RestaurantInput.jsx";
import IngredientInput from "../Ingredient/IngredientInput.jsx";
import {useState} from "react";
import PropTypes from "prop-types";
import FileUpload from "../FileUpload.jsx";

const FrancesinhaForm = ({ currentFrancesinha, closeModal, submitFrancesinha}) => {

    const [name, setName] = useState(currentFrancesinha ? currentFrancesinha.name : "");
    const [price, setPrice] = useState(currentFrancesinha ? currentFrancesinha.price : 0);
    const [rating, setRating] = useState(currentFrancesinha ? currentFrancesinha.rating : 1.0);
    const [ingredients, setIngredients] = useState(currentFrancesinha ? currentFrancesinha.ingredients : []);
    const [restaurant, setRestaurant] = useState(currentFrancesinha ? currentFrancesinha.restaurant : null);

    const [file, setFile] = useState(null);

    const handleClose = (e) => {
        e.preventDefault();

        // Reset form fields
        setName("");
        setPrice(0);
        setRating(1.0);
        setIngredients([]);
        setRestaurant("");

        closeModal();
    }

    const handleSubmitFrancesinha = (e) => {
        e.preventDefault();

        submitFrancesinha({
            name: name,
            price: price,
            rating: rating,
            ingredients: ingredients,
            restaurant: restaurant,
            file: file
        });
    }

    return (
        <form className="space-y-4">

            <div className="flex gap-4">
                <div>
                    <label className="label">
                        <span className="text-base label-text font-semibold">Francesinha Name</span>
                    </label>
                    <input type="text" className="w-full input input-bordered"
                           value={name} onChange={(e) => setName(e.target.value)}/>
                </div>
                <div>

                    <label className="label">
                        <span className="text-base label-text font-semibold">Price (€)</span>
                    </label>
                    <input type="number" className="input input-bordered"
                           value={price} onChange={(e) => setPrice(e.target.value)}/>
                </div>
            </div>

            <div>
                <label className="label">
                    <span className="text-base label-text font-semibold">Rating</span>
                </label>
                <input type="number" className="w-full input input-bordered" min="1" max="5" step="0.1"
                       value={rating} onChange={(e) => setRating(e.target.value)}/>
            </div>

            {/*<div>*/}
            {/*    <label className="label">*/}
            {/*        <span className="text-base label-text font-semibold">Restaurant</span>*/}
            {/*    </label>*/}
            {/*    <input type="text" className="w-full input input-bordered"*/}
            {/*           value={restaurant} onChange={(e) => setRestaurant(e.target.value)}/>*/}
            {/*</div>*/}
            <RestaurantInput addRestaurant={setRestaurant} currentRestaurant={restaurant ? restaurant.name : ""}/>

            {/*<div>*/}
            {/*    <label className="label">*/}
            {/*        <span className="text-base label-text font-semibold">Ingredients</span>*/}
            {/*    </label>*/}
            {/*    <input type="text" className="w-full input input-bordered"*/}
            {/*           value={restaurant} onChange={(e) => setRestaurant(e.target.value)}/>*/}
            {/*</div>*/}
            <IngredientInput setIngredients={setIngredients} addedIngredients={ingredients}/>

            <FileUpload setFile={setFile}/>

            <div className="flex flex-row justify-end gap-4 pt-6">
                <button className="btn"
                        onClick={handleClose}>Cancel
                </button>
                <button className="btn btn-primary"
                        onClick={handleSubmitFrancesinha}>Submit Francesinha
                </button>
            </div>
        </form>
    )
}

// Props Validation
FrancesinhaForm.propTypes = {
    currentFrancesinha: PropTypes.object,
    closeModal: PropTypes.func.isRequired,
    submitFrancesinha: PropTypes.func.isRequired
}

export default FrancesinhaForm;