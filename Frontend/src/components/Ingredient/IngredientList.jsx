import IngredientCard from "./IngredientCard.jsx";
import PropTypes from "prop-types";

const IngredientList = ({ ingredients }) => {

    return (
        <div className="flex flex-col gap-4">
            {ingredients.map(ingredient => (
                <IngredientCard key={ingredient.id} ingredient={ingredient}/>
            ))}

            {ingredients.length === 0 && <div className="opacity-50 text-sm">No ingredients found.</div>}
        </div>
    )
}

// Props Validation
IngredientList.propTypes = {
    ingredients: PropTypes.array.isRequired
}

export default IngredientList;