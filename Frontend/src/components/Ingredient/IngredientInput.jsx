import {useIngredients} from "../../hooks/useIngredients.jsx";
import {useEffect, useState} from "react";
import PropTypes from "prop-types";
import {IoIosClose} from "react-icons/io";
import {Link} from "react-router-dom";

const IngredientInput = ({addedIngredients, setIngredients}) => {

    const [selectedIngredient, setSelectedIngredient] = useState(null);
    const {data: ingredients, error, isLoading} = useIngredients();

    useEffect(() => {
        if (!ingredients || ingredients.length === 0) {
            return;
        }

        setSelectedIngredient(ingredients[0].id);
    }, [ingredients]);

    if (isLoading) {
        return <div>Loading...</div>
    }

    if (error) {
        return <div>Error: {error.message}</div>
    }

    const handleAddIngredient = (e) => {

        e.preventDefault();

        if (!selectedIngredient) {
            return;
        }

        const ingredient = ingredients.find(ingredient => ingredient.id === selectedIngredient);
        setIngredients([...addedIngredients, ingredient]);
    }

    const handleRemoveIngredient = (index) => {
        const newIngredients = addedIngredients.filter((ingredient, i) => i !== index);
        setIngredients(newIngredients);
    }

    if (ingredients.length === 0) {
        return (
            <div>
                <label className="label">
                    <span className="text-base label-text font-semibold">Ingredients</span>
                </label>
                <div>
                    <p className="text-xs ml-1">No ingredients available,
                        create <Link to="/ingredients"
                                     className="text-primary hover:underline">here</Link></p>
                </div>
            </div>
        )
    }

    return (
        <div>
            <label className="label">
                <span className="text-base label-text font-semibold">Ingredients</span>
            </label>
            <div className="flex flex-col gap-2">
                <div className="flex flex-wrap gap-1">
                    {addedIngredients && addedIngredients.map((ingredient, index) => (
                        <div key={ingredient.id} className="badge badge-primary cursor-pointer pr-1"
                             onClick={() => handleRemoveIngredient(index)}>
                            {ingredient.name} <IoIosClose/>
                        </div>
                    ))}
                </div>
                <div className="flex gap-2">
                    <select className="select select-bordered font-semibold w-full"
                            onChange={(e) => setSelectedIngredient(e.target.value)}>
                        {ingredients.map(ingredient => (
                            <option key={ingredient.id} value={ingredient.id}>{ingredient.name}</option>
                        ))}
                    </select>
                    <button className="btn btn-primary" onClick={handleAddIngredient}>Add</button>
                </div>
            </div>
        </div>
    )

}

// Props Validation
IngredientInput.propTypes = {
    addedIngredients: PropTypes.array.isRequired,
    setIngredients: PropTypes.func.isRequired
}

export default IngredientInput;