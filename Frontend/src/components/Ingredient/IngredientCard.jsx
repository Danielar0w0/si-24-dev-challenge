import PropTypes from "prop-types";

const IngredientCard = ({ ingredient }) => {

    return (
        <div className="flex justify-between items-center rounded-lg p-4 bg-base-200/15">
            <div className="flex flex-col">
                <h2 className="text-xl font-bold">{ingredient.name}</h2>
            </div>
        </div>
    )
}

// Props Validation
IngredientCard.propTypes = {
    ingredient: PropTypes.object.isRequired
}

export default IngredientCard;