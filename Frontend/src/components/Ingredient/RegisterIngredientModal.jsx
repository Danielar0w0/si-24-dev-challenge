import {IoCloseCircle} from "react-icons/io5";
import {useCreateIngredient} from "../../hooks/useIngredients.jsx";
import {useState} from "react";

const RegisterIngredientModal = () => {

    const createIngredientMutation = useCreateIngredient();

    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);

    const [name, setName] = useState("");

    const closeModal = (e) => {
        e.preventDefault();

        // Reset form fields
        setName("");

        document.getElementById("create_ingredient_modal").close();
    }

    const submitIngredient = (e) => {

        e.preventDefault();

        setError(null);

        if (name === "") {
            setTimeout(() => setError("Please fill out all fields"), 200);
            return;
        }

        setLoading(true);
        createIngredientMutation.mutate({
            name: name
        });

        setLoading(false);
    }

    return (
        <dialog id="create_ingredient_modal" className="modal modal-bottom sm:modal-middle">
            <div className="modal-box p-12 flex flex-col">
                <form method="dialog" className="modal-close" onClick={closeModal}>
                    <button className="btn btn-sm btn-circle btn-ghost absolute right-4 top-4">✕</button>
                </form>
                <div className="flex flex-col gap-4">
                    <h1 className="text-5xl font-bold">Register Francesinha</h1>
                    <p className="opacity-50 text-sm mb-4">Fill out the form to register a new francesinha.</p>

                    {loading &&
                        <div className="flex items-center gap-2 p-4 rounded-xl w-full bg-base-200/30 font-semibold">
                            <span className="loading loading-spinner loading-sm"></span>
                            <p>Registering francesinha...</p>
                        </div>
                    }
                    {error &&
                        <div
                            className="flex flex-row items-center gap-2 p-4 rounded-xl w-full bg-base-200/30 font-semibold">
                            <IoCloseCircle className="w-6 h-6 text-red-500"/> {error}
                        </div>
                    }

                    <form className="space-y-4">
                        <div>
                            <label className="label">
                                <span className="text-base label-text font-semibold">Ingredient Name</span>
                            </label>
                            <input type="text" className="w-full input input-bordered"
                                   value={name} onChange={(e) => setName(e.target.value)}/>
                        </div>
                    </form>
                    <div className="flex flex-row justify-end gap-4 mt-8">
                        <button className="btn" disabled={loading}
                                onClick={closeModal}>Cancel
                        </button>
                        <button className="btn btn-primary" disabled={loading}
                                onClick={submitIngredient}>Register Ingredient
                        </button>
                    </div>
                </div>
            </div>
            <form method="dialog" className="modal-backdrop fixed top-0 left-0 w-full h-full"
                  onClick={closeModal}>
                <button>close</button>
            </form>
        </dialog>
    )

}

export default RegisterIngredientModal;

