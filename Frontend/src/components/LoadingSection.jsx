
const LoadingSection = () => {

    return (
        <div className="flex-1 flex flex-col justify-center items-center">
            <span className="loading loading-ring loading-lg"></span>
            <p className="font-bold mt-4">Loading...</p>
        </div>
    )
}

export default LoadingSection;