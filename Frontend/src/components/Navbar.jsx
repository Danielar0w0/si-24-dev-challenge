import {Link} from "react-router-dom";

const Navbar = () => {

    return (
        <div className="navbar flex flex-wrap bg-base-100">
            <div className="flex-1">
                <Link to="/" className="btn btn-ghost text-xl font-bold">
                    <img src="/Logo.svg" alt="Francesinha" className="w-8 h-8 mr-2"/>
                    Francesinhoo</Link>
            </div>
            <div className="flex-none">
                <ul className="menu menu-horizontal px-1">
                    <li><Link to="/francesinhas" className="btn btn-ghost">Francesinhas</Link></li>
                    <li><Link to="/restaurants" className="btn btn-ghost">Restaurants</Link></li>
                    <li><Link to="/ingredients" className="btn btn-ghost">Ingredients</Link></li>
                </ul>
            </div>
        </div>
    )
}

export default Navbar;