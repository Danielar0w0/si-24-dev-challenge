import {useEffect, useState} from "react";
import PropTypes from "prop-types";

const RestaurantForm = ({restaurant, closeModal, submitRestaurant}) => {

    const [name, setName] = useState("");
    const [address, setAddress] = useState("");
    const [city, setCity] = useState("");
    const [country, setCountry] = useState("");
    const [rating, setRating] = useState(1.0);

    useEffect(() => {
        if (restaurant) {
            setName(restaurant.name);
            setAddress(restaurant.address);
            setCity(restaurant.city);
            setCountry(restaurant.country);
            setRating(restaurant.rating);
        }
    }, [restaurant]);

    const handleClose = (e) => {
        e.preventDefault();

        // Reset form fields
        setName("");
        setAddress("");
        setCity("");
        setCountry("");
        setRating(1.0);

        closeModal();
    }

    const handleSubmit = (e) => {
        e.preventDefault();

        submitRestaurant({
            name: name,
            address: address,
            city: city,
            country: country,
            rating: rating
        });
    }

    return (
        <form className="space-y-4">

            <div className="flex gap-4">
                <div>
                    <label className="label">
                        <span className="text-base label-text font-semibold">Restaurant Name</span>
                    </label>
                    <input type="text" onChange={(e) => setName(e.target.value)}
                           className="w-full input input-bordered" required value={name}/>
                </div>
                <div>
                    <label className="label">
                        <span className="text-base label-text font-semibold">Address</span>
                    </label>
                    <input type="text" onChange={(e) => setAddress(e.target.value)}
                           className="w-full input input-bordered" required value={address}/>
                </div>
            </div>

            <div className="flex gap-4">
                <div>
                    <label className="label">
                        <span className="text-base label-text font-semibold">City</span>
                    </label>
                    <input type="text" onChange={(e) => setCity(e.target.value)}
                           className="w-full input input-bordered" required value={city}/>
                </div>
                <div>
                    <label className="label">
                        <span className="text-base label-text font-semibold">Country</span>
                    </label>
                    <input type="text" onChange={(e) => setCountry(e.target.value)}
                           className="w-full input input-bordered" required value={country}/>
                </div>
            </div>

            <div>
                <label className="label">
                    <span className="text-base label-text font-semibold">Rating</span>
                </label>
                <input type="number" className="w-full input input-bordered" min="1" max="5" step="0.1"
                       value={rating} onChange={(e) => setRating(e.target.value)}/>
            </div>

            <div className="flex flex-row justify-end gap-4 mt-8">
                <button className="btn" onClick={handleClose}>Cancel
                </button>
                <button className="btn btn-primary" onClick={handleSubmit}>Submit Restaurant
                </button>
            </div>
        </form>
    )
}

// Props Validation
RestaurantForm.propTypes = {
    restaurant: PropTypes.object,
    closeModal: PropTypes.func.isRequired,
    submitRestaurant: PropTypes.func.isRequired
}

export default RestaurantForm;