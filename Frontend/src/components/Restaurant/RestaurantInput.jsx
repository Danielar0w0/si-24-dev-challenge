import {useRestaurants} from "../../hooks/useRestaurants.jsx";
import {useEffect, useState} from "react";
import {Link} from "react-router-dom";
import PropTypes from "prop-types";

const RestaurantInput = ({addRestaurant, currentRestaurant}) => {

    const [restaurantName, setRestaurantName] = useState(currentRestaurant ? currentRestaurant : "");
    const [filteredRestaurants, setFilteredRestaurants] = useState([]);

    const {data: restaurants, error, isLoading} = useRestaurants();

    useEffect(() => {
        if (!restaurants) {
            return;
        }

        let filtered = restaurants.filter(restaurant => {
            return restaurant.name.toLowerCase().includes(restaurantName.toLowerCase());
        });

        setFilteredRestaurants(filtered);

    }, [restaurantName, restaurants]);

    useEffect(() => {

        if (restaurantName && restaurants) {
            const currentRestaurant = restaurants.find(restaurant => restaurant.name === restaurantName);
            if (currentRestaurant)
                addRestaurant(currentRestaurant);
            else
                addRestaurant(null);
        }

    }, [restaurantName, restaurants, addRestaurant]);

    if (isLoading) {
        return <div>Loading...</div>
    }

    if (error) {
        return <div>Error: {error.message}</div>
    }

    if (restaurants.length === 0) {
        return (
            <div>
                <label className="label">
                    <span className="text-base label-text font-semibold">Restaurant</span>
                </label>
                <div>
                    <p className="text-xs ml-1">No restaurants available,
                        create <Link to="/restaurants"
                                     className="text-primary hover:underline">here</Link>
                    </p>
                </div>
            </div>
        )
    }

    return (
        <div className="dropdown z-[1] w-full">
            <div>
                <label className="label">
                    <span className="text-base label-text font-semibold">Restaurant</span>
                </label>
                <input tabIndex={0} type="text" className="w-full input input-bordered"
                       value={restaurantName} onChange={(e) => setRestaurantName(e.target.value)}/>
            </div>
            <ul tabIndex={0}
                className="bg-base-100 dropdown-content dropdown-open p-2 border mt-4 rounded-box w-full overflow-y-auto"
                style={{maxHeight: "100px"}}>
                {filteredRestaurants && filteredRestaurants.map((restaurant, index) => {
                    return (
                        <li key={index} tabIndex={index + 1}
                            className="btn btn-ghost justify-start w-full"
                            onClick={() => setRestaurantName(restaurant.name)}>
                            {restaurant.name}
                        </li>
                    )
                })}
            </ul>
        </div>
    )
}

// Props Validation
RestaurantInput.propTypes = {
    addRestaurant: PropTypes.func.isRequired,
    currentRestaurant: PropTypes.string
}

export default RestaurantInput;