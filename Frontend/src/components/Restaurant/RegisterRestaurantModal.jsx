import {useState} from "react";
import {IoCloseCircle} from "react-icons/io5";
import {useCreateRestaurant} from "../../hooks/useRestaurants.jsx";
import RestaurantForm from "./RestaurantForm.jsx";

const RegisterRestaurantModal = () => {

    const createRestaurantMutation = useCreateRestaurant();

    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);

    const closeModal = (e) => {
        e.preventDefault();

        setLoading(false);
        document.getElementById("create_restaurant_modal").close();
    }

    const submitRestaurant = (restaurant) => {

        setError(null);

        if (restaurant.name === "" || restaurant.address === "" || restaurant.city === "" || restaurant.country === "") {
            setTimeout(() => setError("Please fill out all fields"), 200);
            return;
        }

        // Check if rating is between 1 and 5
        if (restaurant.rating < 1 || restaurant.rating > 5) {
            setTimeout(() => setError("Rating must be between 1 and 5"), 200);
            return;
        }

        setLoading(true);
        createRestaurantMutation.mutate({
            name: restaurant.name,
            address: restaurant.address,
            city: restaurant.city,
            country: restaurant.country,
            rating: restaurant.rating
        });

        setLoading(false);
    }

    return (
        <dialog id="create_restaurant_modal" className="modal modal-bottom sm:modal-middle">
            <div className="modal-box p-12 flex flex-col">
                <form method="dialog" className="modal-close" onClick={closeModal}>
                    <button className="btn btn-sm btn-circle btn-ghost absolute right-4 top-4">✕</button>
                </form>
                <div className="flex flex-col gap-4">
                    <h1 className="text-5xl font-bold">Register Restaurant</h1>
                    <p className="opacity-50 text-sm mb-4">Fill out the form to register a new restaurant.</p>

                    {loading &&
                        <div className="flex items-center gap-2 p-4 rounded-xl w-full bg-base-200/30 font-semibold">
                            <span className="loading loading-spinner loading-sm"></span>
                            <p>Registering restaurant...</p>
                        </div>
                    }
                    {error &&
                        <div
                            className="flex flex-row items-center gap-2 p-4 rounded-xl w-full bg-base-200/30 font-semibold">
                            <IoCloseCircle className="w-6 h-6 text-red-500"/> {error}
                        </div>
                    }

                    <RestaurantForm closeModal={closeModal} submitRestaurant={submitRestaurant} />

                </div>
            </div>
            <form method="dialog" className="modal-backdrop fixed top-0 left-0 w-full h-full"
                  onClick={closeModal}>
                <button>close</button>
            </form>
        </dialog>
    )
        ;
}

export default RegisterRestaurantModal;