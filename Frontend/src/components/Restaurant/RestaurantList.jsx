import RestaurantCard from "./RestaurantCard.jsx";
import PropTypes from "prop-types";

const RestaurantList = ({restaurants}) => {

    return (
        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-12">
            {restaurants?.map((restaurant, index) => (
                <RestaurantCard key={index} restaurant={restaurant}/>
            ))}

            {(restaurants === undefined || restaurants.length === 0) && <div className="opacity-50 text-sm">No restaurants found.</div>}
        </div>
    )
}

// Props Validation
RestaurantList.propTypes = {
    restaurants: PropTypes.array.isRequired
}

export default RestaurantList;