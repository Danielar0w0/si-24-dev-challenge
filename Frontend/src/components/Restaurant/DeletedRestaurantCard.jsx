import {FaStar} from "react-icons/fa";
import {useRestoreRestaurant} from "../../hooks/useRestaurants.jsx";
import PropTypes from "prop-types";

const DeletedRestaurantCard = ({restaurant}) => {

    const restoreRestaurantMutation = useRestoreRestaurant(restaurant.id);

    const handleRestoreRestaurant = (id) => {
        restoreRestaurantMutation.mutate(id);
    }

    return (
        <div onClick={() => handleRestoreRestaurant(restaurant.id)}
              className="flex flex-col gap-4 rounded-lg cursor-pointer">
            <div className="w-full h-56 bg-base-200 hover:bg-primary/80 rounded-t-lg flex items-end justify-end p-3">
                <div className="bg-base-300 rounded-lg p-2">
                    <div className="flex gap-1 items-center font-bold text-sm">{restaurant.rating.toFixed(1)} <FaStar /></div>
                </div>
            </div>
            <div className="flex flex-col gap-1">
                <h2 className="text-3xl font-bold">{restaurant.name}</h2>
                <p className="text-xs">{restaurant.address}</p>
            </div>
        </div>
    )
}

// Props Validation
DeletedRestaurantCard.propTypes = {
    restaurant: PropTypes.object.isRequired
}

export default DeletedRestaurantCard;