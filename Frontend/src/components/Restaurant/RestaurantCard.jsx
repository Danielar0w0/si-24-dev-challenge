import {Link} from "react-router-dom";
import {FaStar} from "react-icons/fa";
import PropTypes from "prop-types";

const RestaurantCard = ({restaurant}) => {

    return (
        <Link to={"/restaurants/" + restaurant.id}
              className="flex flex-col gap-4 rounded-lg cursor-pointer">
            <div className="w-full h-56 bg-base-200 rounded-t-lg flex items-end justify-end p-3">
                <div className="bg-base-300 rounded-lg p-2">
                    <div className="flex gap-1 items-center font-bold text-sm">{restaurant.rating.toFixed(1)} <FaStar /></div>
                </div>
            </div>
            <div className="flex flex-col gap-1">
                <h2 className="text-3xl font-bold">{restaurant.name}</h2>
                <p className="text-xs">{restaurant.address}</p>
            </div>
        </Link>
    )
}

// Props Validation
RestaurantCard.propTypes = {
    restaurant: PropTypes.object.isRequired
}

export default RestaurantCard;