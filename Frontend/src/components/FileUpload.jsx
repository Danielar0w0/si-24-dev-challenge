import PropTypes from "prop-types";

const FileUpload = ({setFile}) => {

    return (
        <div>
            <label className="label">
                <span className="text-base label-text font-semibold">Upload Image</span>
            </label>
            <input type="file" className="file-input file-input-bordered w-full"
                   onChange={(e) => setFile(e.target.files[0])}/>
        </div>
    );
};

// Props Validation
FileUpload.propTypes = {
    setFile: PropTypes.func.isRequired
}

export default FileUpload;
