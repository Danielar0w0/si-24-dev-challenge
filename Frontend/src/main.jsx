import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './index.css'
import {createBrowserRouter, RouterProvider} from "react-router-dom";
import Home from "./pages/Home.jsx";
import ErrorPage from "./pages/ErrorPage.jsx";
import Francesinhas from "./pages/Francesinhas.jsx";
import Restaurants from "./pages/Restaurants.jsx";
import Francesinha from "./pages/Francesinha.jsx";
import {QueryClient, QueryClientProvider} from "react-query";
import Ingredients from "./pages/Ingredients.jsx";
import Restaurant from "./pages/Restaurant.jsx";
import DeletedRestaurants from "./pages/DeletedRestaurants.jsx";
import DeletedFrancesinhas from "./pages/DeletedFrancesinhas.jsx";

const queryClient = new QueryClient();

const router = createBrowserRouter([
    {
        path: "/",
        element: <App/>,
        errorElement: <ErrorPage/>,
        children: [
            {
                path: "/",
                element: <Home/>,
            },
            {
                path: "/francesinhas",
                element: <Francesinhas/>,
            },
            {
                path: "/francesinhas/deleted",
                element: <DeletedFrancesinhas/>,
            },
            {
                path: "/francesinhas/:id",
                element: <Francesinha/>,
            },
            {
                path: "/restaurants",
                element: <Restaurants/>,
            },
            {
                path: "/restaurants/deleted",
                element: <DeletedRestaurants/>,
            },
            {
                path: "/restaurants/:id",
                element: <Restaurant/>,
            },
            {
                path: "/ingredients",
                element: <Ingredients/>,
            }
        ],
    },
]);

ReactDOM.createRoot(document.getElementById('root')).render(
    <React.StrictMode>
        {/*<App />*/}
        <QueryClientProvider client={queryClient}>
            <RouterProvider router={router}/>
        </QueryClientProvider>
    </React.StrictMode>,
)
