import { useQuery, useMutation, useQueryClient } from 'react-query';
import {
    getIngredients,
    getIngredient,
    createIngredient,
    updateIngredient,
    deleteIngredient,
    restoreIngredient
} from '../api/IngredientApiHandler';
import {toast} from "react-toastify";

export const useIngredients = () => {
    return useQuery('ingredients', getIngredients);
};

export const useIngredient = (id) => {
    return useQuery(['ingredient', id], () => getIngredient(id));
};

export const useCreateIngredient = () => {
    const queryClient = useQueryClient();
    return useMutation(createIngredient, {
        onSuccess: () => {
            queryClient.invalidateQueries('ingredients');
            toast.success('Ingredient created successfully');
        },
        onError: (error) => {
            if (error.response.status === 500) {
                toast.error('Ingredient already exists');
            }
        }
    });
};

export const useUpdateIngredient = (id) => {
    const queryClient = useQueryClient();
    return useMutation((data) => updateIngredient(id, data), {
        onSuccess: () => {
            queryClient.invalidateQueries(['ingredient', id]);
            queryClient.invalidateQueries('ingredients');
            toast.success('Ingredient updated successfully');
        },
        onError: (error) => {
            if (error.response.status === 500) {
                toast.error('Ingredient already exists');
            }
        }
    });
};

export const useDeleteIngredient = (id) => {
    const queryClient = useQueryClient();
    return useMutation(() => deleteIngredient(id), {
        onSuccess: () => {
            queryClient.invalidateQueries('ingredients');
        }
    });
};

export const useRestoreIngredient = (id) => {
    const queryClient = useQueryClient();
    return useMutation(() => restoreIngredient(id), {
        onSuccess: () => {
            queryClient.invalidateQueries('ingredients');
        }
    });
};