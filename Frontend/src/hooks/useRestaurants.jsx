import {useMutation, useQuery, useQueryClient} from 'react-query';
import {
    createRestaurant,
    deleteRestaurant, getDeletedRestaurants,
    getRestaurant,
    getRestaurantFrancesinhas,
    getRestaurants,
    restoreRestaurant,
    updateRestaurant
} from '../api/RestaurantApiHandler';
import {toast} from "react-toastify";

export const useRestaurants = () => {
    return useQuery('restaurants', getRestaurants);
};

export const useDeletedRestaurants = () => {
    return useQuery('deletedRestaurants', getDeletedRestaurants);
}

export const useRestaurant = (id) => {
    return useQuery(['restaurant', id], () => getRestaurant(id));
};

export const useCreateRestaurant = () => {
    const queryClient = useQueryClient();
    return useMutation(createRestaurant, {
        onSuccess: () => {
            queryClient.invalidateQueries('restaurants');
            toast.success('Restaurant created successfully');

            // setTimeout(() => {
            //     document.getElementById('create_restaurant_modal').close();
            // }, 1000);
        },
        onError: () => {
            toast.error('An error occurred while creating the restaurant, please try again later');
        }
    });
};

export const useUpdateRestaurant = () => {
    const queryClient = useQueryClient();
    return useMutation((data) => updateRestaurant(data), {
        onSuccess: (data) => {
            queryClient.invalidateQueries(['restaurant', data.id]);
            queryClient.invalidateQueries('restaurants');

            toast.success('Restaurant updated successfully');
            // setTimeout(() => {
            //     document.getElementById('update_restaurant_modal').close();
            // }, 1000);
        },
        onError: () => {
            toast.error('An error occurred while updating the restaurant, please try again later');
        }
    });
};

export const useDeleteRestaurant = () => {
    const queryClient = useQueryClient();
    return useMutation((id) => deleteRestaurant(id), {
        onSuccess: () => {
            toast.success('Restaurant deleted successfully');
            setTimeout(() => {
                queryClient.invalidateQueries('deletedRestaurants');
                queryClient.invalidateQueries('restaurants');
                window.location.href = '/restaurants';
            }, 1000);
        },
        onError: () => {
            toast.error('An error occurred while deleting the restaurant, please try again later');
        }
    });
};

export const useRestoreRestaurant = (id) => {
    const queryClient = useQueryClient();
    return useMutation(() => restoreRestaurant(id), {
        onSuccess: () => {
            toast.success('Restaurant restored successfully');

            queryClient.invalidateQueries('deletedRestaurants');
            queryClient.invalidateQueries('restaurants');

        },
        onError: () => {
            toast.error('An error occurred while restoring the restaurant, please try again later');
        }
    });
};

export const useRestaurantFrancesinhas = (id) => {
    return useQuery(['restaurant', id, 'francesinhas'], () => getRestaurantFrancesinhas(id));
}