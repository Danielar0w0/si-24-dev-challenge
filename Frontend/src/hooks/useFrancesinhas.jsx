import { useQuery, useMutation, useQueryClient } from 'react-query';
import {
    getFrancesinhas,
    getFrancesinha,
    createFrancesinha,
    updateFrancesinha,
    deleteFrancesinha,
    restoreFrancesinha, getDeletedFrancesinhas
} from '../api/FrancesinhaApiHandler';
import {toast} from "react-toastify";

export const useFrancesinhas = () => {
    return useQuery('francesinhas', getFrancesinhas);
};

export const useDeletedFrancesinhas = () => {
    return useQuery('deletedFrancesinhas', getDeletedFrancesinhas);
}

export const useFrancesinha = (id) => {
    return useQuery(['francesinha', id], () => getFrancesinha(id));
};

export const useCreateFrancesinha = () => {
    const queryClient = useQueryClient();
    return useMutation((formData) => createFrancesinha(formData), {
        onSuccess: () => {
            queryClient.invalidateQueries('francesinhas');
            toast.success('Francesinha created successfully');
        },
        onError: () => {
            toast.error('An error occurred while creating the francesinha, please try again later');
        }
    });
};

export const useUpdateFrancesinha = () => {
    const queryClient = useQueryClient();
    return useMutation((formData) => updateFrancesinha(formData), {
        onSuccess: (data) => {
            queryClient.invalidateQueries(['francesinha', data.id]);
            queryClient.invalidateQueries('francesinhas');
            toast.success('Francesinha updated successfully');
        },
        onError: () => {
            toast.error('An error occurred while updating the francesinha, please try again later');
        }
    });
};

export const useDeleteFrancesinha = () => {
    const queryClient = useQueryClient();
    return useMutation((id) => deleteFrancesinha(id), {
        onSuccess: () => {
            toast.success('Francesinha deleted successfully');

            setTimeout(() => {
                queryClient.invalidateQueries('francesinhas');
                queryClient.invalidateQueries('deletedFrancesinhas');

                window.location.href = '/francesinhas';
            }, 1000);
        },
        onError: () => {
            toast.error('An error occurred while deleting the francesinha, please try again later');
        }
    });
};

export const useRestoreFrancesinha = () => {
    const queryClient = useQueryClient();
    return useMutation((id) => restoreFrancesinha(id), {
        onSuccess: () => {
            queryClient.invalidateQueries('deletedFrancesinhas');
            toast.success('Francesinha restored successfully');
        },
        onError: () => {
            toast.error('An error occurred while restoring the francesinha, please try again later');
        }
    });
};