import {
    useDeleteRestaurant,
    useRestaurant,
    useRestaurantFrancesinhas,
} from "../hooks/useRestaurants.jsx";
import {useParams} from "react-router-dom";
import {FaStar} from "react-icons/fa";
import {FaLocationDot} from "react-icons/fa6";
import FrancesinhaCard from "../components/Francesinha/FrancesinhaCard.jsx";
import UpdateRestaurantModal from "../components/Restaurant/UpdateRestaurantModal.jsx";
import LoadingSection from "../components/LoadingSection.jsx";

const Restaurant = () => {

    const {id} = useParams();

    const {data: restaurant, error, isLoading} = useRestaurant(id);
    const {
        data: francesinhas,
        error: francesinhasError,
        isLoading: francesinhasIsLoading
    } = useRestaurantFrancesinhas(id);

    const deleteRestaurantMutation = useDeleteRestaurant();

    if (isLoading || francesinhasIsLoading) {
        return <LoadingSection/>
    }

    if (error) {
        return <div>Error: {error.message}</div>
    }

    if (francesinhasError) {
        return <div>Error: {francesinhasError.message}</div>
    }

    const deleteRestaurant = (e) => {
        e.preventDefault();
        deleteRestaurantMutation.mutate(restaurant.id);
    }

    return (
        <div className="flex flex-col gap-8">
            <div className="bg-base-200/30 p-16">
                <div className="container mx-auto grid grid-cols-2 gap-4 gap-y-12 w-full">
                    <div className="flex flex-col justify-center gap-6 col-span-2 md:col-span-1 order-2 md:order-1">
                        <div>
                            <div className="bg-base-300 rounded-lg p-2 w-fit">
                                <div
                                    className="flex gap-1 items-center font-bold text-sm">{restaurant.rating.toFixed(1)}
                                    <FaStar/>
                                </div>
                            </div>
                            <h1 className="text-7xl font-bold break-words">{restaurant.name}</h1>
                        </div>
                        <div>
                            <p className="flex gap-2 items-center"><FaLocationDot/> {restaurant.address}</p>
                            <p className="text-sm">{restaurant.city}, {restaurant.country}</p>
                        </div>
                    </div>

                    <div
                        className="w-full h-96 min-h-[50vh] bg-base-200 rounded-lg flex p-3 col-span-2 md:col-span-1 order-1 md:order-2">
                    </div>
                </div>
            </div>

            <div className="container mx-auto p-8 flex flex-col items-center gap-12 mb-16">
                <div className="flex flex-col md:items-center gap-2">
                    <h1 className="text-5xl font-bold">Francesinhas Menu</h1>
                    <p>Discover the best francesinhas in town.</p>
                </div>
                <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-12">
                    {francesinhas && francesinhas.map(francesinha => (
                        <FrancesinhaCard francesinha={francesinha} key={francesinha.id}/>
                    ))}
                </div>
            </div>

            <div className="flex bg-base-200/30 justify-end gap-4 p-8">
                <button className="btn"
                        onClick={deleteRestaurant}>Delete
                </button>
                <button className="btn btn-primary"
                        onClick={() => document.getElementById("update_restaurant_modal").showModal()}>Edit Restaurant
                </button>
            </div>

            <UpdateRestaurantModal currentRestaurant={restaurant}/>
        </div>
    )
}

export default Restaurant;