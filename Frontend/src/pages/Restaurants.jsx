import RestaurantList from "../components/Restaurant/RestaurantList.jsx";
import RegisterRestaurantModal from "../components/Restaurant/RegisterRestaurantModal.jsx";
import {useEffect, useState} from "react";
import {useRestaurants} from "../hooks/useRestaurants.jsx";
import {Link} from "react-router-dom";
import LoadingSection from "../components/LoadingSection.jsx";


const Restaurants = () => {

    const [filteredRestaurants, setFilteredRestaurants] = useState([]);

    const [search, setSearch] = useState("");
    const [sort, setSort] = useState("rating");

    const {data: restaurants, error, isLoading} = useRestaurants();

    useEffect(() => {

        if (!restaurants) {
            return;
        }

        let filtered = restaurants.filter(restaurant => {
            return restaurant.name.toLowerCase().includes(search.toLowerCase());
        });

        if (sort === "rating") {
            filtered = filtered.sort((a, b) => b.rating - a.rating);
        } else if (sort === "name") {
            filtered = filtered.sort((a, b) => a.name.localeCompare(b.name));
        }

        setFilteredRestaurants(filtered);

    }, [search, sort, restaurants]);

    if (isLoading) {
        return <LoadingSection/>
    }

    if (error) {
        return <div>Error: {error.message}</div>
    }

    return (
        <div className="flex flex-col container mx-auto gap-12 p-8">

            <div className="flex flex-wrap justify-between gap-y-8">
                <div className="flex flex-col gap-2">
                    <h1 className="text-5xl font-bold">Explore Restaurants</h1>
                    <p>Find the perfect dining experience for you.</p>
                </div>
                <div className="flex flex-wrap gap-4">
                    <Link to="/restaurants/deleted" className="btn">Deleted Restaurants</Link>

                    <button className="btn btn-primary"
                            onClick={() => document.getElementById('create_restaurant_modal').showModal()}>Create
                        Restaurant
                    </button>
                    <select className="select select-bordered font-semibold" onChange={(e) => setSort(e.target.value)}>
                        <option value="rating">Sort by rating</option>
                        <option value="name">Sort by name</option>
                    </select>

                    <input type="text" placeholder="Search for a restaurant..."
                           className="input input-bordered" onChange={(e) => setSearch(e.target.value)}/>
                </div>
            </div>

            {/*<button className="btn"*/}
            {/*        onClick={() => document.getElementById('create_restaurant_modal').showModal()}>Create Restaurant*/}
            {/*</button>*/}

            <RestaurantList restaurants={filteredRestaurants}/>
            <RegisterRestaurantModal/>
        </div>
    )
}

export default Restaurants;