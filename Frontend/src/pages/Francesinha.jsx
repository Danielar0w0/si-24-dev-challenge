import {Link, useParams} from "react-router-dom";
import {useDeleteFrancesinha, useFrancesinha} from "../hooks/useFrancesinhas.jsx";
import {FaStar} from "react-icons/fa";
import {FaLocationDot} from "react-icons/fa6";
import UpdateFrancesinhaModal from "../components/Francesinha/UpdateFrancesinhaModal.jsx";
import LoadingSection from "../components/LoadingSection.jsx";

const Francesinha = () => {

    const {id} = useParams();

    const {data: francesinha, error, isLoading} = useFrancesinha(id);

    const deleteFrancesinhaMutation = useDeleteFrancesinha();

    const deleteFrancesinha = (e) => {
        e.preventDefault();
        deleteFrancesinhaMutation.mutate(francesinha.id);
    }

    if (isLoading) {
        return <LoadingSection/>
    }

    if (error) {
        return <div>Error: {error.message}</div>
    }

    if (!francesinha) {
        return <div>Francesinha not found</div>
    }

    return (
        <div className="flex flex-col flex-1 bg-base-200/30 p-16">
            <div className="container mx-auto grid grid-cols-2 gap-4 gap-y-12 w-full">
                <div className="flex flex-col justify-center gap-6 col-span-2 md:col-span-1 order-2 md:order-1">
                    <div>
                        <div className="bg-base-300 rounded-lg p-2 w-fit">
                            <div
                                className="flex gap-1 items-center font-bold text-sm">{francesinha.rating.toFixed(1)}
                                <FaStar/>
                            </div>
                        </div>
                        <h1 className="text-7xl font-bold break-words">{francesinha.name}</h1>
                    </div>

                    <div>
                        <p className="flex flex-wrap gap-2 items-center"><FaLocationDot/>
                            <Link to={"/restaurants/" + francesinha.restaurant.id} className="font-bold">
                                {francesinha.restaurant.name}</Link> {francesinha.restaurant.address}</p>
                        <p className="text-sm">{francesinha.restaurant.city}, {francesinha.restaurant.country}</p>
                    </div>

                    <div>
                        {francesinha.ingredients.map(ingredient => (
                            <div key={ingredient.id} className="flex gap-2 items-center">
                                <div className="w-3 h-3 bg-primary rounded-full"></div>
                                <p>{ingredient.name}</p>
                            </div>
                        ))}
                    </div>
                </div>

                <div
                    className="w-full h-96 min-h-[50vh] bg-base-200 rounded-lg flex p-3 col-span-2 md:col-span-1 order-1 md:order-2"
                    style={{backgroundImage: `url(data:image/jpeg;base64,${francesinha.photo})`, backgroundSize: "cover"}}>
                </div>
            </div>

            <div className="flex justify-end gap-4 mt-14 md:mt-auto">
                <button className="btn"
                        onClick={deleteFrancesinha}>Delete
                </button>
                <button className="btn btn-primary"
                        onClick={() => document.getElementById("update_francesinha_modal").showModal()}>Edit Francesinha
                </button>
            </div>

            <UpdateFrancesinhaModal currentFrancesinha={francesinha}/>
        </div>
    )

}

export default Francesinha;