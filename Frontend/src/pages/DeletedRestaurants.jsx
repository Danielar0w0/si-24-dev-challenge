import {useDeletedRestaurants} from "../hooks/useRestaurants.jsx";
import DeletedRestaurantCard from "../components/Restaurant/DeletedRestaurantCard.jsx";
import LoadingSection from "../components/LoadingSection.jsx";

const DeletedRestaurants = () => {

    const {data: deletedRestaurants, error, isLoading} = useDeletedRestaurants();

    if (isLoading) {
        return <LoadingSection/>
    }

    if (error) {
        return <div>Error: {error.message}</div>
    }

    return (
        <div className="flex flex-col container mx-auto gap-12 p-8">

            <div className="flex flex-wrap justify-between gap-y-8">
                <div className="flex flex-col gap-2">
                    <h1 className="text-5xl font-bold">Recover Restaurants</h1>
                    <p>Here you can recover deleted restaurants.</p>
                </div>
            </div>
            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-12">
                {deletedRestaurants?.map((restaurant, index) => (
                    <DeletedRestaurantCard restaurant={restaurant} key={index}/>
                ))}

                {(!deletedRestaurants || deletedRestaurants.length === 0) &&
                    <div className="opacity-50 text-sm">No restaurants found.</div>}
            </div>
        </div>
    )
}

export default DeletedRestaurants;