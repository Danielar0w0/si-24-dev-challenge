import FrancesinhaList from "../components/Francesinha/FrancesinhaList.jsx";
import {useFrancesinhas} from "../hooks/useFrancesinhas.jsx";
import {useEffect, useState} from "react";
import RegisterFrancesinhaModal from "../components/Francesinha/RegisterFrancesinhaModal.jsx";
import {Link} from "react-router-dom";
import LoadingSection from "../components/LoadingSection.jsx";

const Francesinhas = () => {

    const [filteredFrancesinhas, setFilteredFrancesinhas] = useState([]);

    const [search, setSearch] = useState("");
    const [sort, setSort] = useState("rating");

    // const filteredFrancesinhas = [
    //     {
    //         name: "Mega Francesinha",
    //         price: 10,
    //         rating: 4.9,
    //         ingredients: ["Ham", "Steak", "Egg", "Cheese", "Bacon", "Sausage", "Bread", "Francesinha sauce"],
    //         restaurant: "A Biquinha"
    //     },
    //     {
    //         name: "Super Frances",
    //         price: 10,
    //         rating: 4.9,
    //         ingredients: ["Ham", "Steak", "Egg", "Cheese", "Bacon", "Sausage", "Bread", "Francesinha sauce"],
    //         restaurant: "A Biquinha"
    //     }
    // ];

    const {data: francesinhas, error, isLoading} = useFrancesinhas();

    useEffect(() => {

            if (!francesinhas) {
                return;
            }

            let filtered = francesinhas.filter(francesinha => {
                return francesinha.name.toLowerCase().includes(search.toLowerCase());
            });

            if (sort === "rating") {
                filtered = filtered.sort((a, b) => b.rating - a.rating);
            } else if (sort === "name") {
                filtered = filtered.sort((a, b) => a.name.localeCompare(b.name));
            }

            setFilteredFrancesinhas(filtered);

    }, [search, sort, francesinhas]);

    if (isLoading) {
        return <LoadingSection/>
    }

    if (error) {
        return <div>Error: {error.message}</div>
    }

    return (
        <div className="flex flex-col container mx-auto gap-12 p-8">

            <div className="flex flex-wrap justify-between gap-y-8">
                <div className="flex flex-col gap-2">
                    <h1 className="text-5xl font-bold">Explore Francesinhas</h1>
                    <p>Find the perfect francesinha for you.</p>
                </div>
                <div className="flex flex-wrap gap-4">
                    <Link to="/francesinhas/deleted" className="btn">Deleted Francesinhas</Link>
                    <button className="btn btn-primary"
                            onClick={() => document.getElementById('create_francesinha_modal').showModal()}>Create
                        Francesinha
                    </button>
                    <select className="select select-bordered font-semibold" onChange={(e) => setSort(e.target.value)}>
                        <option value="rating">Sort by rating</option>
                        <option value="name">Sort by name</option>
                    </select>

                    <input type="text" placeholder="Search for a francesinha..."
                           className="input input-bordered" onChange={(e) => setSearch(e.target.value)}/>
                </div>
            </div>

            <FrancesinhaList francesinhas={filteredFrancesinhas}/>
            <RegisterFrancesinhaModal/>
        </div>
    )

}

export default Francesinhas;