import {useIngredients} from "../hooks/useIngredients.jsx";
import IngredientList from "../components/Ingredient/IngredientList.jsx";
import {useEffect, useState} from "react";
import RegisterIngredientModal from "../components/Ingredient/RegisterIngredientModal.jsx";

const Ingredients = () => {

    const [search, setSearch] = useState("");
    const [filteredIngredients, setFilteredIngredients] = useState([]);

    const {data: ingredients, isLoading, isError, error} = useIngredients();

    useEffect(() => {
        if (!ingredients) {
            return;
        }

        let filtered = ingredients.filter(ingredient => {
            return ingredient.name.toLowerCase().includes(search.toLowerCase());
        });

        setFilteredIngredients(filtered);
    }, [search, ingredients]);

    if (isLoading) {
        return <div>Loading...</div>
    }

    if (isError) {
        return <div>Error: {error.message}</div>
    }

    return (
        <div className="flex flex-col container mx-auto gap-12 p-8">
            <div className="flex flex-wrap justify-between gap-y-8">
                <div className="flex flex-col gap-2">
                    <h1 className="text-5xl font-bold">Explore Ingredients</h1>
                    <p>Discover the best ingredients to create the perfect francesinha!</p>
                </div>
                <div className="flex flex-wrap gap-4">
                    <button className="btn btn-primary"
                            onClick={() => document.getElementById('create_ingredient_modal').showModal()}>Create
                        Ingredient
                    </button>

                    <input type="text" placeholder="Search for an ingredient..."
                           className="input input-bordered" onChange={(e) => setSearch(e.target.value)}/>
                </div>
            </div>
            <IngredientList ingredients={filteredIngredients}/>
            <RegisterIngredientModal/>
        </div>
    )

}

export default Ingredients;