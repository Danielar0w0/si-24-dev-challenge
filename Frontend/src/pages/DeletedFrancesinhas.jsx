import {useDeletedFrancesinhas} from "../hooks/useFrancesinhas.jsx";
import DeletedFrancesinhaCard from "../components/Francesinha/DeletedFrancesinhaCard.jsx";
import LoadingSection from "../components/LoadingSection.jsx";

const DeletedFrancesinhas = () => {

    const {data: deletedFrancesinhas, error, isLoading} = useDeletedFrancesinhas();

    if (isLoading) {
        return <LoadingSection/>
    }

    if (error) {
        return <div>Error: {error.message}</div>
    }

    return (
        <div className="flex flex-col container mx-auto gap-12 p-8">

            <div className="flex flex-wrap justify-between gap-y-8">
                <div className="flex flex-col gap-2">
                    <h1 className="text-5xl font-bold">Recover Francesinhas</h1>
                    <p>Here you can recover deleted francesinhas.</p>
                </div>
            </div>
            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-12">
                {deletedFrancesinhas?.map((francesinha, index) => (
                    <DeletedFrancesinhaCard francesinha={francesinha} key={index}/>
                ))}

                {(!deletedFrancesinhas || deletedFrancesinhas.length === 0) &&
                    <div className="opacity-50 text-sm">No francesinhas found.</div>}
            </div>
        </div>
    )
}

export default DeletedFrancesinhas;