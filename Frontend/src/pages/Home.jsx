
const Home = () => {

    return (
        <div className="grid lg:grid-cols-2 flex-1 items-center container mx-auto gap-12 p-8">
            <div className="flex flex-col gap-2">
                <h1 className="text-7xl font-bold break-words">Francesinha</h1>
                <p>A traditional Portuguese sandwich made with bread, <br/>meat, cheese, and a special sauce.</p>
            </div>
            <div className="flex-1">
                <img src="/francesinha.jpg" alt="Francesinha" className="rounded-lg"/>
            </div>
        </div>
    )
}

export default Home;