import {useRouteError} from "react-router-dom";

const ErrorPage = () => {

    const error = useRouteError();
    console.error(error);

    return (
        <div className="container mx-auto flex flex-col items-center gap-2 h-screen justify-center">
            <h1 className="text-4xl font-bold">Oops!</h1>
            <p>Sorry, an unexpected error has occurred.</p>
            <p className="mt-4">
                <i>{error.statusText || error.message}</i>
            </p>
        </div>
    );
}

export default ErrorPage;